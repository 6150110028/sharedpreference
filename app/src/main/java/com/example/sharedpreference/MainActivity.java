package com.example.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    CheckBox chk1;
    Button btn1 ,btn2,btn3;
    EditText edt1;
    Set<String> mySet= new HashSet<String>();
    Set<String>numbers = new HashSet<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chk1 = (CheckBox) findViewById(R.id.checkBox1);
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        edt1 = (EditText) findViewById(R.id.editText1);
        final SharedPreferences shared = getSharedPreferences("MY_PREFS",
                Context.MODE_PRIVATE);
        boolean checkBoxValue = shared.getBoolean("CheckBox_Value", false);
        String name = shared.getString("storedName", "YourName");
        numbers = shared.getStringSet("mySet",null);
        if (numbers!=null){
            for (String num : numbers) {
                Log.i("SharedPref","Number :"+num);
    }
}
        mySet.add("One");
        mySet.add("Two");
        mySet.add("Three");
        mySet.add("Four");
        mySet.add("Five");
        mySet.add("Six");
        mySet.add("Seven");
        if(checkBoxValue) {
            chk1.setChecked(true);
        } else {
            chk1.setChecked(false);
        }
        edt1.setText(name);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = shared.edit();
                editor.putBoolean("CheckBox_Value",chk1.isChecked());
                if (chk1.isChecked()) {
                    editor.putString("storedName",edt1.getText().toString());
                    editor.putStringSet("mySet",mySet);
                }
                editor.commit();
            }
            });
             btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                SharedPreferences.Editor editor = shared.edit();
                editor.putBoolean("CheckBox_Value", chk1.isChecked());
                if (chk1.isChecked()) {
                    editor.putString("storedName", edt1.getText().toString());
                    editor.putStringSet("mySet", mySet);
                }
                editor.commit();
            }
        });
        btn3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                SharedPreferences.Editor editor = shared.edit();
                editor.clear();
                if (chk1.isChecked()) {
                    editor.putString("storedName", edt1.getText().toString());
                    editor.putStringSet("mySet", mySet);
                }
                editor.commit();
            }
        });
    }
}